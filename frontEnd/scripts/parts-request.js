function renderPartsInTable(partsList) {
    document.getElementById("parts-table").hidden = false;
    const tableBody = document.getElementById("parts-table-body");
    for (let part of partsList) {
        let partNumber = part.partNumber;
        let newRow = document.createElement("tr");
        newRow.setAttribute("id", "tr-" + partNumber);
        let plusButtonId = "plus-" + partNumber
        let minusButtonId = "minus-" + partNumber
        let deleteButtonId = "delete-" + partNumber
        newRow.innerHTML = `<td>${part.partName}</td><td>${part.retailPrice}</td>
                            <td>
                            <div>
                                <div id=${plusButtonId} style="display: inline" onclick="getPartId(this.id)">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-plus-square-fill" viewBox="0 0 16 16">
                                        <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3a.5.5 0 0 1 1 0z" />
                                    </svg>
                                </div>                                
                                <div id=${minusButtonId} style="display: inline" onclick="getPartId(this.id)">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-dash-square-fill" viewBox="0 0 16 16">
                                        <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm2.5 7.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1 0-1z" />
                                    </svg>
                                </div>
                            </div>
                            </td>
                            <td>
                            <div id=${deleteButtonId} onclick="deletePart(this.id)">
                                 <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                 </svg>
                            </div>
                            </td>`;
        tableBody.appendChild(newRow);
    }
}

function successCallBack(xhr) {
    console.log("Successfully accessed data");
    const parts = JSON.parse(xhr.responseText);
    renderPartsInTable(parts);
}

function failureCallBack(xhr) {
    console.log("Parts data not accessesed, failed!");
}

ajaxPopulatePartsList("http://localhost:7000/parts", successCallBack, failureCallBack, "token");

function failureDeleteCallBack(xhr) {
    console.log("Could not delete the part");
}

function successDeleteCallBack(xhr) {
    console.log("Deleting a part");
}

function deletePart(id) {
    let partId = id.match(/\d+/)[0];
    let rowId = "tr-" + partId;
    var row = document.getElementById(rowId);
    row.hidden = true;
    ajaxDeletePart(partId, successDeleteCallBack, failureDeleteCallBack, "token");
}
