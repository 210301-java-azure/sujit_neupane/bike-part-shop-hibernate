document.getElementById("add-new-part-form").addEventListener("submit", addNewPart);

function addNewPart(event) {
    event.preventDefault();
    const partName = document.getElementById("inputPartName").value;
    const retailPrice = document.getElementById("inputRetailPrice").value;
    const newPart = { "partName": partName, "retailPrice": retailPrice };
    ajaxAddNewPart(newPart, successCallBack, failureCallBack, "token");
}

function successCallBack(xhr) {
    const msg = document.getElementById("partAddedMsg");
    msg.hidden = false;
    msg.innerText = "Added New Part Successfully";
}

function failureCallBack(xhr) {
    const msg = document.getElementById("partAddedMsg");
    msg.hidden = false;
    msg.innerText = xhr.responseText;
}