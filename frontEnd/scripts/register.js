document.getElementById("registerForm").addEventListener("submit", register);

function register(event) {
    event.preventDefault();
    const fullName = document.getElementById("inputFullName").value;
    const phoneNumber = document.getElementById("inputPhoneNumber").value;
    const email = document.getElementById("inputEmail").value;
    const password = document.getElementById("inputPassword").value;
    ajaxRegister(fullName, phoneNumber, email, password, successfulRegister, registerFailed);
}

function successfulRegister(xhr) {
    console.log("yay success registration");
    window.location.href = "../templates/login.html";
}

function registerFailed(xhr) {
    console.log("failed registration");
}
