function sendAjaxRequest(method, url, body, successCallback, failureCallback, authToken) {
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);
    if (authToken) {
        xhr.setRequestHeader("Authorization", authToken);
    }
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status > 199 && xhr.status < 300) {
                console.log("Successfully connected!!")
                successCallback(xhr);
            } else {
                failureCallback(xhr);
            }
        }
    }
    if (body) {
        xhr.send(body);
    } else {
        xhr.send();
    }
}

function sendAjaxPost(url, body, successCallback, failureCallback, authToken) {
    sendAjaxRequest("POST", url, body, successCallback, failureCallback, authToken);
}

function ajaxPopulatePartsList(url, successCallBack, failureCallBack, authToken) {
    sendAjaxRequest("GET", url, undefined, successCallBack, failureCallBack, authToken);
}

function ajaxRegister(fullName, phoneNumber, email, password, successCallBack, failureCallback) {
    const payload = `fullName=${fullName}&phoneNumber=${phoneNumber}&email=${email}&password=${password}`;
    sendAjaxPost("http://localhost:7000/register", payload, successCallBack, failureCallback);
}

function ajaxLogin(email, password, successCallback, failureCallback) {
    const payload = `email=${email}&password=${password}`;
    sendAjaxPost("http://localhost:7000/login", payload, successCallback, failureCallback);
}

function ajaxAddNewPart(newPart, successCallBack, failureCallBack, authToken) {
    const part = JSON.stringify(newPart);
    sendAjaxPost("http://localhost:7000/parts", part, successCallBack, failureCallBack, authToken);
}

function ajaxDeletePart(partId, successDeleteCallBack, failureDeleteCallBack, authToken) {
    sendAjaxRequest("DELETE", "http://localhost:7000/parts/" + partId, undefined, successDeleteCallBack, failureDeleteCallBack, authToken);
}
