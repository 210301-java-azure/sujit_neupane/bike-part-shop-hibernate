document.getElementById("login-form").addEventListener("submit", attemptLogin);

function attemptLogin(event) {
    event.preventDefault(); // this stops the form from sending a GET request by default to the same URL
    const email = document.getElementById("inputEmail1").value;
    const password = document.getElementById("inputPassword1").value;
    ajaxLogin(email, password, successfulLogin, loginFailed);
}

function successfulLogin(xhr) {
    console.log("yay success");
    const authToken = xhr.getResponseHeader("Authorization");
    sessionStorage.setItem("token", authToken);
    window.location.href = "../templates/base.html";
}

function loginFailed(xhr) {
    const errorDiv = document.getElementById("error-msg");
    errorDiv.hidden = false;
    errorDiv.innerText = xhr.responseText;
}

