function getPartId(id) {
    let buttonId = id;
    document.getElementById("no-part-msg").hidden = true;
    //check if plus or minus and perform operation accordingly
    if (checkPlus(buttonId)) {       
        addPartToInvoice(buttonId);
    } else {      
        removePartFromInvoice(buttonId);
    }    
}

function checkPlus(buttonId) {
    if (buttonId.match(/^p/)) {
        return true;
    } else {
        return false;
    }
}

function removePartFromInvoice(buttonId) {
    let partId = buttonId.match(/\d+/)[0];
    const rowId = "tr-" + partId + "-i";

    const invoiceTable = document.getElementById("invoice-table");
    let rowCount = invoiceTable.rows.length;
    if (rowCount == 2) { // table is empty
        document.getElementById("no-part-msg").hidden = false;
    } else { // there are parts in the table
        tr = invoiceTable.getElementsByTagName("tr"); // get all the table rows
        for (let i = 0; i < tr.length - 1; i++) { // i is a row number in invoice-table, 
            // length-1 is used to avoid the row with total
            if (tr[i].getAttribute("id") == rowId) {
                var row = document.getElementById(rowId);
                let currentPartQuantity = parseInt(row.childNodes[1].innerText);
                if (currentPartQuantity > 1) {
                    // handling row's price and quantities
                    let currentPartQuantity = parseInt(row.childNodes[1].innerText);
                    let currentPartPrice = parseFloat(row.childNodes[2].innerText);
                    let newQuantity = currentPartQuantity - 1;
                    let partPrice = currentPartPrice / currentPartQuantity;
                    let newPartPrice = currentPartPrice - partPrice;
                    row.childNodes[1].innerText = newQuantity;
                    row.childNodes[2].innerText = newPartPrice;

                    // adjusting total row
                    let currentQuantity = parseInt(document.getElementById("part-quantity-data").innerText);
                    let currentTotalPrice = parseFloat(document.getElementById("invoice-total-data").innerText);
                    let newTotalQuantity = currentQuantity - 1;            
                    document.getElementById("part-quantity-data").innerText = newTotalQuantity;
                    document.getElementById("invoice-total-data").innerText = currentTotalPrice - partPrice;
                    
                } else { // adjust total row and remove the node
                    let currentQuantity = parseInt(document.getElementById("part-quantity-data").innerText);
                    let currentTotalPrice = parseFloat(document.getElementById("invoice-total-data").innerText);
                    let newQuantity = currentQuantity - 1;
                    let partPrice = parseFloat(row.childNodes[2].innerText);
                    document.getElementById("part-quantity-data").innerText = newQuantity;
                    document.getElementById("invoice-total-data").innerText = currentTotalPrice - partPrice;
                    row.parentNode.removeChild(row);
                }
            }
        }        
    }
}

function addPartToInvoice(buttonId) {
    document.getElementById("invoice-table-caption").hidden = true;
    // extracting number from button id. As number comprised of part number 
    // and is used to make all elements related to this part number
    let partId = buttonId.match(/\d+/)[0];
    const rowId = "tr-" + partId; // making row-id along with part number    
    const partPrice = parseFloat(document.getElementById(rowId).childNodes[1].innerText);
    const partName = document.getElementById(rowId).childNodes[0].innerText;

    let partQuantity = 1;
    let newRow = document.createElement("tr");
    let newRowId = rowId + "-i";

    // get the table
    const invoiceTable = document.getElementById("invoice-table");
    let rowCount = invoiceTable.rows.length;
    var wasDuplicate = false;
    if (rowCount > 1) { // if the table is not empty
        tr = invoiceTable.getElementsByTagName("tr"); // get all the table rows
        for (let i = 0; i < tr.length; i++) { // i is a row number in invoice-table
            if (tr[i].getAttribute("id") == newRowId) { // if table row contains id matching newRowId
                // adjust quantity part
                let currentCount = document.getElementById(newRowId).childNodes[1].innerText; // get the currentCount from the table
                let newCount = parseInt(currentCount) + partQuantity; // add new quantity to currentcount to get new count
                document.getElementById(newRowId).childNodes[1].innerText = newCount; // set the value of the quantity column related to the part to newCount

                // adjust price part
                let currentPrice = document.getElementById(newRowId).childNodes[2].innerText; // get the current price
                let newPrice = parseFloat(currentPrice) + partPrice; // add new price to current price
                document.getElementById(newRowId).childNodes[2].innerText = newPrice; // set the price to new Price
                wasDuplicate = true;
            }         
        }
    }
    if (!wasDuplicate) { // if the added part was not already in the table then add it to invoice table 
        newRow.setAttribute("id", newRowId);
        newRow.innerHTML = `<td>${partName}</td><td>${partQuantity}</td><td>${partPrice}</td>`;
        const tableBody = document.getElementById("invoice-table-body");
        tableBody.appendChild(newRow);
    }

    // update total row's data
    let totalRow = document.getElementById("invoice-total");
    totalRow.hidden = false;    
    let currentCount = document.getElementById("part-quantity-data").innerText;
    if (currentCount == 0) {
        document.getElementById("part-quantity-data").innerText = partQuantity;
        document.getElementById("invoice-total-data").innerText = partPrice;
    } else {
        let newCount = parseInt(currentCount) + parseInt(partQuantity);
        let currentPrice = document.getElementById("invoice-total-data").innerText;
        newPrice = parseInt(currentPrice) + parseInt(partPrice);
        document.getElementById("part-quantity-data").innerText = newCount;
        document.getElementById("invoice-total-data").innerText = newPrice;
    }
}