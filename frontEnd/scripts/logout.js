// JavaScript source code
document.getElementById("logout").addEventListener("onclick", logout);

function logout(event) {
    event.preventDefault();
    sessionStorage.removeItem("token");
    window.location.href = "../templates/login.html";
    console.log("Logout Success");
}
