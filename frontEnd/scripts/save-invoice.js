document.getElementById("save-invoice-button").addEventListener("submit", saveInvoice);

function saveInvoice(event) {
    event.preventDefault();
    const invoiceTable = document.getElementById("invoice-table");
    var tr = invoiceTable.getElementByTagName("tr"); //getting all the rows in the invoice table
    for (let i = 0; i < tr.length - 1; i++) {
        //get part name and part price and make a new part object
        var partDataRow = tr[i].getElementByTagName("td");
        var partName = partDataRow[0];
        var retailPrice = partDataRow[2];
        var quantity = partDataRow[1];
        var part = { "partName": partName, "retailPrice": retailPrice };
        var invoicePart = { "part": part, "quantity": quantity };
        var employee = { "fullName": "S N", "phoneNumber": "0000", "emailAddress": "sn@gmail.com" };
        var customer = { "fullName": "P B", "phoneNumber": "1111", "emailAddress": "pb@gmail.com" };
        var date = new Date();
    }
}
