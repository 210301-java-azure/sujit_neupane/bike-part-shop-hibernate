/*
create table part(
	id serial primary key,
	part_number Integer,
	part_name varchar(100),
	retail_price numeric(6, 2)
)

create table customer(
	id serial primary key,
	full_name varchar(50),
	phone_number varchar(14),
	email_address varchar(50)
)

 create table invoice(
	id serial primary key,
	invoice_number Integer,
	invoice_date Date,
	customer_id Integer references customer(id) on delete set null,
	employee_id Integer references employee(id) on delete set null
)
create table employee(
	id serial primary key,
	full_name varchar(50),
	phone_number varchar(14),
	email_address varchar(50)
)

create table part_invoice(
	invoice_id Integer references invoice(id) on delete set null,
	part_id Integer references part(id) on delete set null, 	
	quantity Integer
)

create table user_credentials(
	id serial primary key,
	email varchar(50) unique,
	pword varchar(100)
)

truncate table invoice, part_invoice

alter table invoice 
add constraint invoice_number unique (invoice_number)

truncate table part, part_invoice 
alter table part
add constraint part_number unique (part_number)

 select * from employee e 
 where e.full_name = 'Bind Budhathoki' and e.email_address = 'bind@gmail.com'
*/

alter table user_credentials 
add column salt varchar(100)

truncate table user_credentials 
