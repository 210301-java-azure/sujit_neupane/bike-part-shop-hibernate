**Bike-Part-Shop Web App**

**Project Description**
- To be able to use this web app, one has to create an account using 'Register'
and login with the created credentials.
- In this web app, one can add and query bike parts. You are also able to add and 
remove parts to and from invoice table using plus and minus icon displayed in parts table.
- Delete icon will delete part from the database and removes from parts table.

**Technologies Used:**
 REST, Java, HTML, CSS, JavaScript, Git, Javalin, Azure, DevOps, SQL, JDBC, Hibernate

**Features:**
- User can sign up and login to the web app
- User can see all the parts displayed from the database
- User can search part in a parts table
- User can add and delete parts to and from parts table
- User can add and remove parts to the invoice table using plus and minus icon

**To-do list:**
- From invoice table, user will be able to process added parts as a customer order
- JWT token will be used to provide better user security
- Payment API will be implemented to process a payment


_**Contributor**_
 Sujit Neupane
