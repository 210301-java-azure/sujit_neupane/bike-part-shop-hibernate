FROM java:8
COPY build/libs/bike-parts-shop-1.0-SNAPSHOT.jar .
EXPOSE 80
CMD java -jar bike-parts-shop-1.0-SNAPSHOT.jar