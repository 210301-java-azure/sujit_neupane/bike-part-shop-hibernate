package dev.neu.controllers;

import dev.neu.models.Part;
import dev.neu.service.PartService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class PartControllerTest {

    @InjectMocks
    private PartController partController;

    @Mock
    private PartService service;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @Disabled
    public void testHandleGetAllParts() {
        Context context = mock(Context.class);

        List<Part> parts = new ArrayList<>();
        parts.add(new Part("Saddle", 20));

        when(service.getAllParts()).thenReturn(parts);
        partController.handleGetAllParts(context);
        verify(context).json(parts);
    }
}
