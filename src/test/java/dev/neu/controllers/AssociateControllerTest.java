package dev.neu.controllers;

import dev.neu.models.Associate;
import dev.neu.models.Employee;
import dev.neu.service.AssociateService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;


public class AssociateControllerTest {
    @InjectMocks
    private AssociateController associateController;

    @Mock
    private AssociateService associateService;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @Disabled
    public void testHandleGetAllEmployees() {
        Context context = mock(Context.class);

        List<Associate> employees = new ArrayList<>();
        employees.add(new Employee("Sam Hit", "2000003000", "sam@gmail.com", true));
        when(associateService.getAllEmployees()).thenReturn(employees);
        associateController.handleGetAllEmployees(context);
        verify(context).json(employees);
    }
}
