package dev.neu.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.neu.BikePartShopApp;
import dev.neu.models.Associate;
import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class AssociateControllerIntegrationTest {

    private static BikePartShopApp app = new BikePartShopApp();

    @BeforeAll
    public static void startService() {
        app.start(7000);
    }

    @AfterAll
    public static void stopService() {
        app.stop();
    }

    @Test
    // @Disabled
    public void testGetAllEmployeesAuthorized() {
        HttpResponse<List<Associate>> response = Unirest.get("http://localhost:7000/employees")
                .header("Authorization", "token")
                .asObject(new GenericType<List<Associate>>() {});
        assertAll(
                ()->assertEquals(200, response.getStatus()),
                ()->assertTrue(response.getBody().size()>=0)
        );
    }

    @Test
    public void testAddAssociate() throws JsonProcessingException {
        Associate associate = new Associate("S N", "423232", "sn@gmail.com", true);
        HttpResponse<Associate> response = Unirest.post("http://localhost:7000/employees")
                .body(new ObjectMapper().writeValueAsString(associate))
                .header("Authorization", "token")
                .asObject(Associate.class);
        assertEquals(true, response.getBody().isEmployee());
        /*assertAll(
                ()->assertEquals(true, response.getBody().isEmployee()),
                ()->assertEquals(201, response.getStatus())
        );*/
    }
}
