package dev.neu.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.neu.BikePartShopApp;
import dev.neu.models.Part;
import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PartControllerIntegrationTest {

    private static BikePartShopApp app = new BikePartShopApp();

    @BeforeAll
    public static void startService() {
        app.start(7000);
    }

    @AfterAll
    public static void stopService() {
        app.stop();
    }

    @Test
    //@Disabled
    public void testGetAllPartsAuthorized() {
        HttpResponse<List<Part>> response = Unirest.get("http://localhost:7000/parts")
                .header("Authorization", "token")
                .asObject(new GenericType<List<Part>>() {});
        assertAll(
                ()->assertEquals(200, response.getStatus()),
                ()->assertTrue(response.getBody().size()>=0)
        );
    }

    @Test
    public void testAddPartAuthorized() throws JsonProcessingException {
        Part part = new Part("Helmet", 100);
        HttpResponse<Part> response = Unirest.post("http://localhost:7000/parts")
                .body(new ObjectMapper().writeValueAsString(part))
                .header("Authorization", "token")
                .asObject(Part.class);
        assertAll(
                ()->assertEquals(201, response.getStatus())
                // ()->assertEquals("Helmet", response.getBody().getPartName())
        );
    }

    @Test
    @Disabled
    public void testDeletePart() throws JsonProcessingException {
        Part part = new Part("Helmet", 100);
        HttpResponse<Part> response = Unirest.post("http://localhost:7000/parts")
                .body(new ObjectMapper().writeValueAsString(part))
                .header("Authorization", "token")
                .asObject(Part.class);
        Unirest.delete("http://localhost:7000/parts")
                .body(new ObjectMapper().writeValueAsString(part))
                .header("Authorization", "token")
                .asObject(Part.class);
    }
}
