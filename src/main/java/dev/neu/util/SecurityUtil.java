package dev.neu.util;

import io.javalin.http.Context;

public class SecurityUtil {

    public void attachResponseHeaders(Context ctx) {
        ctx.header("Access-Control-Expose_Headers", "Authorization");
        // ctx.header("Access-Control-Allow-Origin", "*"); // this is what the JavalinConfig::enableCorsForAllOrigins
    }
}
