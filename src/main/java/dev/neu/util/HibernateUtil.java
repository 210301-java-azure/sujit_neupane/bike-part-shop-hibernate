package dev.neu.util;


import dev.neu.models.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class HibernateUtil {

    private static SessionFactory sessionFactory;

    private static SessionFactory getSessionFactory() {
        if (sessionFactory==null) {
            Configuration configuration = new Configuration();
            Properties settings = new Properties();
            settings.put(Environment.URL, System.getenv("connectionUrl"));
            settings.put(Environment.USER, System.getenv("username"));
            settings.put(Environment.PASS, System.getenv("password"));

            settings.put(Environment.DRIVER, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
            settings.put(Environment.DIALECT, "org.hibernate.dialect.SQLServerDialect");

            // we can use update, validate, etc. in place of create below
            settings.put(Environment.HBM2DDL_AUTO, "validate");
            settings.put(Environment.SHOW_SQL, "true");

            configuration.setProperties(settings);

            //provide hibernate mappings to configuration
            configuration.addAnnotatedClass(Part.class);
            configuration.addAnnotatedClass(Associate.class);
            configuration.addAnnotatedClass(InvoicePart.class);
            configuration.addAnnotatedClass(Invoice.class);
            configuration.addAnnotatedClass(UserCredential.class);

            sessionFactory = configuration.buildSessionFactory();
        }
        return sessionFactory;
    }

    public static Session getSession() {
        return getSessionFactory().openSession();
    }
}
