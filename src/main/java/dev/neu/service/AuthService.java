package dev.neu.service;

import dev.neu.data.AuthDaoHib;
import dev.neu.models.Associate;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.SQLException;

public class AuthService {

    private AuthDaoHib authDao = new AuthDaoHib();
    private AssociateService associateService = new AssociateService();

    public boolean authenticateLogin(String email, String password) throws NoSuchProviderException, NoSuchAlgorithmException {
        return authDao.authenticateLogin(email, password);
    }

    public Associate employeeRegistration(Associate associate, String securePassword, String salt) throws SQLException {
        Associate addedAssociate = associateService.addAssociate(associate);
        authDao.registerEmployee(addedAssociate.getEmailAddress(), securePassword, salt);
        return addedAssociate;
    }
}
