package dev.neu.service;

import dev.neu.data.InvoiceDaoHib;
import dev.neu.models.Invoice;

import java.util.List;

public class InvoiceService {

    private InvoiceDaoHib invoiceDao = new InvoiceDaoHib();

    public Invoice addInvoice(Invoice invoice) {
        return invoiceDao.addInvoice(invoice);
    }

    public List<Invoice> getInvoicesByAssociate(String name, String employee) {
        return invoiceDao.getInvoicesByAssociate(name, employee);
    }

    public List<Invoice> getAllInvoices() {
        return invoiceDao.getAllInvoices();
    }
}
