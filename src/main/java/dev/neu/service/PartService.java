package dev.neu.service;

import dev.neu.data.PartDaoHib;
import dev.neu.models.Part;

import java.util.List;

public class PartService {

    private PartDaoHib partDao = new PartDaoHib();

    public PartService() {
        super();
    }

    public Part addPart(Part part) {
        return partDao.addPart(part);
    }

    public List<Part> getAllParts() {
        return partDao.getAllParts();
    }

    public Part getPartByPartNumber(int partNum) {
        return partDao.getPartByPartNumber(partNum);
    }

    public void deletePartByName(String partName) {
        partDao.deletePartByName(partName);
    }

    public void updateRetailPrice(int partNumber, double newPrice) {
        partDao.updateRetailPrice(partNumber, newPrice);
    }

    public Part getPartByPartName(String partName) {
        return partDao.getPartByPartName(partName);
    }

    public void deletePartById(int idInput) {
        partDao.deletePartById(idInput);
    }
}
