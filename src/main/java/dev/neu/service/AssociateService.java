package dev.neu.service;

import dev.neu.data.AssociateDaoHib;
import dev.neu.models.Associate;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class AssociateService {

    private AssociateDaoHib associateDao = new AssociateDaoHib();
    private Logger logger = LoggerFactory.getLogger(AssociateService.class);

    public Associate addAssociate(Associate associate) {
        return associateDao.addAssociate(associate);
    }

    public List<Associate> getAllCustomers() {
        return associateDao.getAllCustomers();
    }

    public List<Associate> getAllEmployees() {
        return associateDao.getAllEmployees();
    }

    public List<Associate> getAllAssociates() {
        return getAllAssociates();
    }

    public Associate getAssociateByName(String fullName, boolean employee) {
        return associateDao.getAssociateByName(fullName, employee);
    }

    public void deleteAssociate(int id) {
        associateDao.deleteAssociate(id);
    }

    public void updatePhoneNumber(String phoneNumber, String associateName,boolean employee) {
        if (associateDao.getAssociateIdByName(associateName, employee) == 0) {
            logger.info("Associate {} is not on record", associateName);
            throw new NotFoundResponse("Associate " + associateName + " is not on record");
        }
        associateDao.updatePhoneNumber(phoneNumber, associateName, employee);
    }
}
