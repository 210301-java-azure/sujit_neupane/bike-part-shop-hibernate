package dev.neu;

import dev.neu.data.InvoiceDaoHib;
import dev.neu.data.InvoicePartDaoHib;
import dev.neu.data.InvoicePartDaoInterface;
import dev.neu.data.PartDaoHib;
import dev.neu.models.Associate;
import dev.neu.models.Invoice;
import dev.neu.models.InvoicePart;
import dev.neu.models.Part;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class DriverHib {

    public static void main(String[] args) {
        Part part = new Part("Saddle", 30);

        PartDaoHib partDaoHib = new PartDaoHib();
        Part addedPart = partDaoHib.addPart(part);
        System.out.println("Part Number: " + addedPart.getPartNumber());
        InvoicePart newIP = new InvoicePart(1, part, 3);
        InvoicePartDaoInterface invoicePartDao = new InvoicePartDaoHib();
        invoicePartDao.addInvoicePart(newIP);

        Associate e = new Associate(1, "S N", "040304", "sn@gmail.com", true); // an employee
        Associate c = new Associate(2, "P B", "04030434", "pb@gmail.com", false); // an employee
        List<InvoicePart> invoiceParts = new ArrayList<>();
        invoiceParts.add(newIP);
        Invoice i = new Invoice(invoiceParts, LocalDateTime.now(), e, c);
        InvoiceDaoHib invoiceDaoHib = new InvoiceDaoHib();
        invoiceDaoHib.addInvoice(i);
        // System.out.println(i.toString());
    }
}
