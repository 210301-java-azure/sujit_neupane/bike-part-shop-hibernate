package dev.neu.controllers;

import dev.neu.models.Associate;
import dev.neu.service.AuthService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.sql.SQLException;


public class AuthController {

    private Logger logger = LoggerFactory.getLogger(AuthController.class);
    private AuthService authService = new AuthService();

    public void handleEmployeeRegistration(Context ctx) throws NoSuchAlgorithmException, NoSuchProviderException, SQLException {
        String fullName = ctx.formParam("fullName");
        String phoneNumber = ctx.formParam("phoneNumber");
        String emailAddress = ctx.formParam("email");
        String password = ctx.formParam("password");
        byte[] salt = getSalt();
        String stringSalt = new String(salt);
        if (fullName != null && phoneNumber != null && emailAddress != null && password != null) {
            Associate associate = new Associate(fullName, phoneNumber, emailAddress, true);
            String securePassword = getSecurePasswordWithSalt(password, salt);
            authService.employeeRegistration(associate, securePassword, stringSalt); // returns the associate, not using now
            logger.info("New employee {}'s registration successful!", fullName);
        } else {
            throw new BadRequestResponse("All fields are required");
        }

    }

    public void authenticateLogin(Context ctx) throws NoSuchAlgorithmException, NoSuchProviderException{
        String email = ctx.formParam("email");
        String password = ctx.formParam("password");
        if (email != null && password != null) {
            logger.info("{} attempted login", email);
            if (authService.authenticateLogin(email, password)) {
                logger.info("Successful login by {}", email);
                ctx.header("Authorization", "token"); // need to change it to jwtToken authorization
                ctx.status(200);
                return;
            }
            else {
                logger.info("Incorrect email or password for {}", email);
                throw new UnauthorizedResponse("Incorrect email or password");
            }
        } else {
            throw new UnauthorizedResponse("Please enter your email and password, both required!");
        }
    }

    public void authorizeToken(Context ctx) {
        logger.info("Authorizing token");

        // allowing OPTIONS call made while login
        if (ctx.method().equals("OPTIONS")) {
            return;
        }

        String authHeader = ctx.header("Authorization");

        if (authHeader != null && authHeader.equals("token")) {
            logger.info("Login is authorized");
        } else {
            logger.warn("Authorization failed");
            throw new UnauthorizedResponse();
        }
    }

    public static String getSecurePasswordWithSalt(String passwordToHash, byte[] salt) throws NoSuchAlgorithmException, NoSuchProviderException
    {
        return hashPassword(passwordToHash, salt);
    }

    private static String hashPassword(String passwordToHash, byte[] salt)
    {
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(salt);
            //Get the hash's bytes
            byte[] bytes = md.digest(passwordToHash.getBytes());
            //This bytes[] has bytes in decimal format
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    //Add salt
    private static byte[] getSalt() throws NoSuchAlgorithmException, NoSuchProviderException
    {
        //Always use a SecureRandom generator
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "SUN");
        //Create array for salt
        byte[] salt = new byte[16];
        //Get a random salt
        sr.nextBytes(salt);
        //return salt
        return salt;
    }
}
