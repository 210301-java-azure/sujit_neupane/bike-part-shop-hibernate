package dev.neu.controllers;

import dev.neu.models.Invoice;
import dev.neu.service.InvoiceService;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller for Invoice
 * Implements handle method of Handler interface
 * get and post operations are performed
 */

public class InvoiceController {
    private Logger logger = LoggerFactory.getLogger(InvoiceController.class);
    private InvoiceService invoiceService = new InvoiceService();

    public void handleAddInvoice(Context ctx) {
        Invoice invoice = ctx.bodyAsClass(Invoice.class);
        logger.info("Adding invoice number: {}", invoice.getInvoiceNumber());
        invoiceService.addInvoice(invoice);
        ctx.status(201);
    }

    public void handleGetInvoices(Context ctx) {
        String name = ctx.queryParam("name");
        String employee = ctx.queryParam("employee"); // if this is employee, pass anything to this query param
        if (name == null && employee == null) {
            logger.info("Getting all invoices");
            ctx.json(invoiceService.getAllInvoices());
        } else if (name != null && employee == null) {
            logger.info("Getting customer {}'s invoices", name);
            ctx.json(invoiceService.getInvoicesByAssociate(name, null));
        } else {
            logger.info("Getting employee {}'s invoices", name);
            ctx.json(invoiceService.getInvoicesByAssociate(name, employee));
        }
    }


}
