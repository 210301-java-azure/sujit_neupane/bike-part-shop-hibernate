package dev.neu.data;

import dev.neu.models.Invoice;

import java.util.List;

public interface InvoiceDaoInterface {
    public Invoice addInvoice(Invoice invoice);
    public List<Invoice> getAllInvoices();
    public List<Invoice> getInvoicesByAssociate(String name, String employee);
}
