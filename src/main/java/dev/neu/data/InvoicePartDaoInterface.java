package dev.neu.data;

import dev.neu.models.InvoicePart;

import java.util.List;

public interface InvoicePartDaoInterface {
    public List<InvoicePart> getAllInvoicePart();
    public InvoicePart addInvoicePart(InvoicePart newInvoicePart);
}
