package dev.neu.data;

import dev.neu.models.Associate;
import dev.neu.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class AssociateDaoHib implements AssociateDaoInterface {

    private Logger logger = LoggerFactory.getLogger(AssociateDaoHib.class);

    public List<Associate> getAllAssociates() {
        List<Associate> allAssociates;
        logger.info("Getting all associates");
        try (Session s = HibernateUtil.getSession()) {
            allAssociates = s.createQuery("from Associate", Associate.class).list();
            return allAssociates;
        }
    }

    @Override
    public List<Associate> getAllCustomers() {
        try (Session s = HibernateUtil.getSession()) {
            logger.info("Getting all customers");
            Query<Associate> allCustomerQuery = s.createQuery("from Associate where employee = :employee", Associate.class);
            allCustomerQuery.setParameter("employee", false);
            return allCustomerQuery.list();
        }
    }

    @Override
    public List<Associate> getAllEmployees() {
        try (Session s = HibernateUtil.getSession()) {
            logger.info("Getting all employees");
            Query<Associate> allEmployeeQuery = s.createQuery("from Associate where employee = :employee", Associate.class);
            allEmployeeQuery.setParameter("employee", true);
            return allEmployeeQuery.list();
        }
    }

    @Override
    public Associate addAssociate(Associate associate) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            int id = (int) s.save(associate);
            associate.setId(id);
            logger.info("Added new associate with id: {},", id);
            tx.commit();
        }
        return associate;
    }


    @Override
    public Associate getAssociateByName(String fullName, boolean employee) {
        return null;
    }

    @Override
    public String getAssociateNameById(int id, boolean employee) {
        return null;
    }

    @Override
    public int getAssociateIdByName(String name, boolean employee) {
        return 0;
    }

    public void deleteAssociate(int id) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            s.delete(new Associate(id));
            tx.commit();
        }
    }

    @Override
    public void updatePhoneNumber(String phoneNumber, String associateName, boolean employee) {

    }
}
