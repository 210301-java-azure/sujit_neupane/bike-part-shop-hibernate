package dev.neu.data;

import dev.neu.controllers.AuthController;
import dev.neu.models.UserCredential;
import dev.neu.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

public class AuthDaoHib {
    private Logger logger = LoggerFactory.getLogger(AuthDaoHib.class);

    public boolean registerEmployee(String email, String password, String salt) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            UserCredential userCredential = new UserCredential(email, password, salt);
            int id = (int) s.save(userCredential);
            userCredential.setId(id);
            logger.info("Added new associate's user credentials to db; UC id: {},", id);
            tx.commit();
        }
        return true;
    }

    public boolean authenticateLogin(String email, String password) throws NoSuchAlgorithmException, NoSuchProviderException {
        try (Session s = HibernateUtil.getSession()) {
            Query<UserCredential> userCredentialQuery = s.createQuery("from UserCredential where emailAddress = :email", UserCredential.class);
            userCredentialQuery.setParameter("email", email);
            UserCredential user = userCredentialQuery.list().get(0);
            String salt = user.getSalt();
            byte[] byteSalt = salt.getBytes();
            String hashedPassword = AuthController.getSecurePasswordWithSalt(password, byteSalt);
            if (hashedPassword.equals(user.getPassword())) {
                return true;
            }
        }
        return false;
    }
}
