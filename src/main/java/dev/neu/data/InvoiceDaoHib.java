package dev.neu.data;

import dev.neu.models.Invoice;
import dev.neu.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class InvoiceDaoHib implements InvoiceDaoInterface {

    Logger logger = LoggerFactory.getLogger(InvoiceDaoHib.class);

    @Override
    public Invoice addInvoice(Invoice invoice) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            int id = (int) s.save(invoice);
            invoice.setInvoiceNumber(id);
            logger.info("Added new invoice with id: {},", id);
            tx.commit();

        }
        return invoice;
    }

    @Override
    public List<Invoice> getAllInvoices() {
        try (Session s = HibernateUtil.getSession()) {
            logger.info("Getting all invoices");
            return s.createQuery("from Invoice", Invoice.class).list();
        }
    }

    @Override
    public List<Invoice> getInvoicesByAssociate(String name, String employee) {
        return null;
    }
}
