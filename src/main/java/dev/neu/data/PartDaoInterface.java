package dev.neu.data;

import dev.neu.models.Part;

import java.util.List;

public interface PartDaoInterface {
    public List<Part> getAllParts();

    public Part addPart(Part part);

    public Part getPartByPartNumber(int partNumber);

    public int getPartIdByPartName(String partName);

    public String getPartNameById(int partId);

    public void deletePartByName(String partName);

    public void updateRetailPrice(int partNumber, double newPrice);

    public void deletePartById(int idInput);
}
