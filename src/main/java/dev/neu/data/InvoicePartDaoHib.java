package dev.neu.data;

import dev.neu.models.InvoicePart;
import dev.neu.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class InvoicePartDaoHib implements InvoicePartDaoInterface{

    Logger logger = LoggerFactory.getLogger(InvoicePartDaoHib.class);

    @Override
    public List<InvoicePart> getAllInvoicePart() {
        logger.info("Getting all invoice_part");
        try (Session s = HibernateUtil.getSession()) {
            return s.createQuery("from InvoicePart", InvoicePart.class).list();
        }
    }

    @Override
    public InvoicePart addInvoicePart(InvoicePart newInvoicePart) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            int id = (int) s.save(newInvoicePart);
            newInvoicePart.setId(id);
            logger.info("Adding new invoice_part with id {}", id);
            tx.commit();
            return newInvoicePart;
        }
    }
}
