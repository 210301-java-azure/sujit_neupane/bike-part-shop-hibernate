package dev.neu.data;

import dev.neu.models.Associate;

import java.util.List;

public interface AssociateDaoInterface {
    public Associate addAssociate(Associate associate);
    public List<Associate> getAllCustomers();
    public List<Associate> getAllEmployees();
    public Associate getAssociateByName(String fullName, boolean employee);
    public String getAssociateNameById(int id, boolean employee);
    public int getAssociateIdByName(String name, boolean employee);
    // public void deleteEmployee(String fullName);
    public void updatePhoneNumber(String phoneNumber, String associateName, boolean employee);
}
