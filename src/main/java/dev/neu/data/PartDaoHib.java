package dev.neu.data;

import dev.neu.models.Part;
import dev.neu.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class PartDaoHib implements PartDaoInterface {

    private Logger logger = LoggerFactory.getLogger(PartDaoHib.class);

    @Override
    public List<Part> getAllParts() {
        List<Part> allParts;
        try (Session s = HibernateUtil.getSession()) {
            allParts = s.createQuery("from Part", Part.class).list();
            return allParts;
        }
    }

    @Override
    public Part addPart(Part part) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            int id = (int)s.save(part);
            part.setPartNumber(id);
            tx.commit();
            return part;
        }
    }

    @Override
    public Part getPartByPartNumber(int partNumber) {
        return null;
    }

    @Override
    public int getPartIdByPartName(String partName) {
        return 0;
    }

    @Override
    public String getPartNameById(int partId) {
        return null;
    }

    public Part getPartByPartName(String partName) {
        try (Session s = HibernateUtil.getSession()) {
            Query<Part> partQuery = s.createQuery("from Part where partName like :name", Part.class);
            partQuery.setParameter("name", partName);
            List<Part> parts = partQuery.list();
            if (parts.isEmpty()) {
                return null;
            } else {
                return parts.get(0);
            }
        }
    }

    @Override
    public void deletePartByName(String partName) {
        try (Session s = HibernateUtil.getSession()) {
            Transaction tx = s.beginTransaction();
            Query<Part> partQuery = s.createQuery("delete Part where partName = :name");
            partQuery.setParameter("name", partName);
            partQuery.executeUpdate();
            tx.commit();
        }
    }

    @Override
    public void updateRetailPrice(int partNumber, double newPrice) {

    }

    @Override
    public void deletePartById(int idInput) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            s.delete(new Part(idInput));
            tx.commit();
        }

    }
}
