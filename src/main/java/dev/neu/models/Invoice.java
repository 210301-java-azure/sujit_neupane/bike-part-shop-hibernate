package dev.neu.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@JsonIgnoreProperties("hibernateLazyInitializer")
public class Invoice implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "invoice_id")
    private int invoiceNumber;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<InvoicePart> partsWithQuantity;

    private LocalDateTime invoiceDate;

    @OneToOne
    private Associate employee;
    @OneToOne
    private Associate customer;

    public Invoice() {
        super();
    }

    public Invoice(int invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Invoice(List<InvoicePart> partsWithQuantity, LocalDateTime invoiceDate, Associate employee, Associate customer) {
        this.partsWithQuantity = partsWithQuantity;
        this.invoiceDate = invoiceDate;
        this.employee = employee;
        this.customer = customer;
    }

    public Invoice(int invoiceNumber, List<InvoicePart> partsWithQuantity, LocalDateTime invoiceDate, Associate employee, Associate customer) {
        this.invoiceNumber = invoiceNumber;
        this.partsWithQuantity = partsWithQuantity;
        this.invoiceDate = invoiceDate;
        this.employee = employee;
        this.customer = customer;
    }

    public int getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(int invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public List<InvoicePart> getPartsWithQuantity() {
        return partsWithQuantity;
    }

    public void setPartsWithQuantity(List<InvoicePart> partsWithQuantity) {
        this.partsWithQuantity = partsWithQuantity;
    }

    public LocalDateTime getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(LocalDateTime invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Associate getEmployee() {
        return employee;
    }

    public void setEmployee(Associate employee) {
        this.employee = employee;
    }

    public Associate getCustomer() {
        return customer;
    }

    public void setCustomer(Associate customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Invoice invoice = (Invoice) o;
        return invoiceNumber == invoice.invoiceNumber && Objects.equals(partsWithQuantity, invoice.partsWithQuantity) && Objects.equals(invoiceDate, invoice.invoiceDate) && Objects.equals(employee, invoice.employee) && Objects.equals(customer, invoice.customer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(invoiceNumber, partsWithQuantity, invoiceDate, employee, customer);
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "invoiceNumber=" + invoiceNumber +
                ", partsWithQuantity=" + partsWithQuantity +
                ", invoiceDate=" + invoiceDate +
                ", employee=" + employee +
                ", customer=" + customer +
                '}';
    }
}