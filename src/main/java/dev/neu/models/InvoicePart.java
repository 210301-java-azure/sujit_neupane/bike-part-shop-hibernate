package dev.neu.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@JsonIgnoreProperties("hibernateLazyInitializer")
public class InvoicePart implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // auto generates id(pk) number
    @Column(name="invoice_part_id")
    private int id;
    @OneToOne
    private Part part;
    private int quantity;

    public InvoicePart() {
        super();
    }

    public InvoicePart(Part part, int quantity) {
        this.part = part;
        this.quantity = quantity;
    }

    public InvoicePart(int id, Part part, int quantity) {
        this.id = id;
        this.part = part;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Part getPart() {
        return part;
    }

    public void setPart(Part part) {
        this.part = part;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InvoicePart that = (InvoicePart) o;
        return id == that.id && quantity == that.quantity && part.equals(that.part);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, part, quantity);
    }

    @Override
    public String toString() {
        return "InvoicePart{" +
                "id=" + id +
                ", part=" + part +
                ", quantity=" + quantity +
                '}';
    }
}
