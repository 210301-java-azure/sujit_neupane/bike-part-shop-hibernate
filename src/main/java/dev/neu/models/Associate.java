package dev.neu.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@JsonIgnoreProperties("hibernateLazyInitializer")
public class Associate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="associate_id")
    protected int id;
    protected String fullName;
    protected String phoneNumber;
    protected String emailAddress;
    protected boolean employee;

    public Associate() {
        super();
    }

    public Associate(int id) {
        this.id = id;
    }

    public Associate(String fullName) {
        this.fullName = fullName;
    }

    // this constructor is used to instantiate customer object
    public Associate(String fullName, String phoneNumber, String emailAddress) {
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.employee = false;
    }

    public Associate(String fullName, String phoneNumber, String emailAddress, boolean employee) {
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.employee = employee;
    }

    // this constructor is used to instantiate employee object
    public Associate(int id, String fullName, String phoneNumber, String emailAddress, boolean employee) {
        this.id = id;
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.employee = employee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEmployee() {
        return employee;
    }

    public void set_employee(Boolean employee) {
        employee = employee;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Associate associate = (Associate) o;
        return id == associate.id && employee == associate.employee && fullName.equals(associate.fullName) && phoneNumber.equals(associate.phoneNumber) && emailAddress.equals(associate.emailAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fullName, phoneNumber, emailAddress, employee);
    }

    @Override
    public String toString() {
        return "Associate{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", employee=" + employee +
                '}';
    }
}
